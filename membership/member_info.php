<html>
    <head>
        <meta charset="UTF-8">
        <title>Tickets Record</title>
    </head>
    <body>
        <?php session_start(); session_regenerate_id(); if($_SESSION['login']!='1') { header("location:../index.php");}?>
        <?php
        	if(isset($_POST['StudentID'])&&isset($_POST['submit'])) {
        		echo '<h3>Tickets Application History</h3>';
        		echo '<iframe frameborder="0" width="10000" height="500" src="../tickets/member_application.php?StudentID=' . filter_input(INPUT_POST, 'StudentID') . '" seamless></iframe>';

        		echo '<hr>';

        		echo '<h3>Loan History</h3>';
		        $mysql = new mysqli($_SESSION['host'], $_SESSION['user'], $_SESSION['password'], $_SESSION['db']);
		        $query = "select L.LoanID, C.Title, L.Timestamp as 'Loan', R.Timestamp as 'Return' from loan as L, movie as C, return_loan as R where L.MovieID=C.ID and L.StudentID='".filter_input(INPUT_POST, 'StudentID')."' and R.LoanID=L.LoanID;";
		        $result = $mysql->query($query);
		        if(!$result) {
		            die(mysqli_error($mysql));
		        }
		        echo '<table cellpadding="10%">';
		        echo '<tr><td>LoanID</td><td>Title</td><td>Loan</td><td>Return</td></tr>';
		        while($row=$result->fetch_assoc()) {
		            echo '<tr><td>'.$row['LoanID'].'</td><td>'.$row['Title'].'</td><td>'.$row['Loan'].'</td><td>'.$row['Return'].'</td></tr>';
		        }
		        echo '</table>';

		        echo '<hr>';

		        $query = "select S.Date, S.Title from screening as S, attendance as A where A.ScreeningID=S.Date and A.StudentID='".filter_input(INPUT_POST, 'StudentID')."' order by S.Date;";
		        $result = $mysql->query($query);
		        if(!$result) {
		        	die(mysqli_error($mysql));
		        }
		        echo '<h3>Screening Attendance</h3>';
		        echo '<table cellpadding="10%">';
		        echo '<tr><td>Date</td><td>Title</td></tr>';
		        while($row=$result->fetch_assoc()) {
		        	echo '<tr><td>'.$row['Date'].'</td><td>'.$row['Title'].'</td></tr>';
		        }
		        echo '</table>';

		        echo '<p><a href="../home.php">Back</a></p>';
        	}
        	else {
        		echo '<form name="member_info" action="member_info.php" method="post">';
        		echo '<p>StudentID: <input type="text" name="StudentID"></input><input type="submit" name="submit" value="submit"></input>';
        		echo '</form>';
        		echo '<p><a href="../home.php">Back</a></p>';
        	}